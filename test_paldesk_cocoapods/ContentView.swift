//
//  ContentView.swift
//  test_paldesk_cocoapods
//
//  Created by Saverio Murgia on 09/01/2020.
//  Copyright © 2020 Saverio Murgia. All rights reserved.
//
import Paldesk
import SwiftUI

struct ContentView: View {
    var body: some View {
        Button(action: {
            print("touched")
        }, label: {
            Text("Ciao")
        })
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
